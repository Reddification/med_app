﻿using MedApp.ApiClient;
using MedApp.DomainObjects;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MedApp.View.Viewmodels
{
    public abstract class DataLoadingViewmodel<T> : BaseViewmodel, IDataLoader where T:IComparable
    {
        protected DataLoadingStatus dataStatus = DataLoadingStatus.NotLoaded;
        private ObservableCollection<T> viewData;
        protected Timer refreshTimer;

        public DataLoadingViewmodel(IApiClient ac, int refreshPeriodMilliseconds) : base(ac)
        {
            this.ViewData = new ObservableCollection<T>();
            refreshTimer = new Timer()
            {
                AutoReset = true,
                Enabled = false,
                Interval = refreshPeriodMilliseconds
            };

            refreshTimer.Elapsed += RefreshTimer_Elapsed;
        }

        private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.ReloadData();
        }

        public DataLoadingStatus DataStatus
        {
            get => dataStatus;
            set
            {
                this.dataStatus = value;
                updateProperty(nameof(DataStatus));
            }
        }

        public ObservableCollection<T> ViewData 
        { 
            get => viewData; 
            set
            {
                viewData = value;
                updateProperty(nameof(ViewData));
            }
        }

        public abstract void ReloadData();

        protected void preLoad()
        {
            refreshTimer.Stop(); // а то заебёт
            this.DataStatus = DataLoadingStatus.Loading;
        }

        protected void mergeData(IEnumerable<T> newData)
        {

        }

        protected void postLoad(IEnumerable<T> newData)
        {
            if (newData != null)
            {
                List<T> toAdd = new List<T>();
                List<T> toRemove = new List<T>();

                foreach (var newItem in newData)
                    if (!this.ViewData.Any(a => a.CompareTo(newItem) == 0))
                        toAdd.Add(newItem);

                foreach (var oldItem in ViewData)
                    if (!newData.Any(a => a.CompareTo(oldItem) == 0))
                        toRemove.Add(oldItem);


                App.Current.Dispatcher.BeginInvoke((Action)delegate
                {
                    foreach (var item in toRemove)
                        ViewData.Remove(item);
                    foreach (var item in toAdd)
                        ViewData.Add(item);

                    DataStatus = DataLoadingStatus.Loaded;
                    this.refreshTimer.Start();
                });
            }
            else
            {
                DataStatus = DataLoadingStatus.Loaded;
                this.refreshTimer.Start();
            }
        }

        public override void Open()
        {
            base.Open();
            this.refreshTimer.Start();
        }

        public override void Close()
        {
            base.Close();
            this.refreshTimer.Stop();
        }
    }
}
