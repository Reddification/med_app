﻿using MedApp.ApiClient;
using MedApp.DomainObjects;
using MedApp.DomainObjects.ViewObjects;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace MedApp.View.Viewmodels
{
    public class MainViewmodel : BaseViewmodel, IViewChanger
    {
        private BaseViewmodel subContent;
        private ExpertChatsViewmodel chatsViewmodel;
        private GlobalStatisticsViewmodel globalStatisticsViewmodel;
        private ReportsViewmodel reportsViewmodel;
        private SettingsViewmodel settingsViewmodel;
        private PatientStatisticsViewmodel patientsStatisticViewmodel;
        private SidebarViewmodel sidebarDataContext;

        private Expert expert;
        private List<Profile> patients;

        public event RequestWindowChangeEventHandler RequestWindowChange;

        public MainViewmodel(IApiClient ac) : base(ac)
         {
            this.patients = new List<Profile>(this.apiClient.GetContactPersonList(true));
            //this.chats = this.apiClient.GetChats(true);
            this.expert = apiClient.GetExpertInfo(false);

            this.SidebarDataContext = new SidebarViewmodel(ac, expert.Name, expert.ProfileImage);
            this.SidebarDataContext.RequestWindowChange += SidebarDataContext_RequestWindowChange;

            chatsViewmodel = new ExpertChatsViewmodel(apiClient, expert);
            globalStatisticsViewmodel = new GlobalStatisticsViewmodel(ac);
            settingsViewmodel = new SettingsViewmodel(ac);
            reportsViewmodel = new ReportsViewmodel(ac);
            patientsStatisticViewmodel = new PatientStatisticsViewmodel(ac, patients);

            chatsViewmodel.RequestWindowChange += ChatsViewmodel_RequestWindowChange;
            patientsStatisticViewmodel.RequestWindowChange += PatientsStatisticViewmodel_RequestWindowChange;

            //default tab
            changeView(globalStatisticsViewmodel);
        }

        private void PatientsStatisticViewmodel_RequestWindowChange(object sender, Utilities.RequestWindowChangeEventArgs e)
        {
            (sender as BaseViewmodel).Close();
            switch (e.TargetWindow)
            {
                case Utilities.WindowType.Chats:
                    int userId = Convert.ToInt32(e.Parameter);
                    chatsViewmodel.OpenChatWithUser(userId);
                    changeView(chatsViewmodel);
                    break;
                default:
                    break;
            }
        }

        private void ChatsViewmodel_RequestWindowChange(object sender, Utilities.RequestWindowChangeEventArgs e)
        {
            (sender as BaseViewmodel).Close();
            switch (e.TargetWindow)
            {
                case Utilities.WindowType.PatientStatistics:
                    int userId = Convert.ToInt32(e.Parameter);
                    //Profile patient = this.patients.FirstOrDefault(f => c.Members.Except(new int[] { expert.ProfileId }).Contains(f.Id));
                    //changeView(patientsStatisticViewmodel);
                    //this.patientsStatisticViewmodel.SelectedPatientId = patient.Id;
                    patientsStatisticViewmodel.OpenStatisticsById(userId);
                    //patientsStatisticViewmodel.SelectedPatientId = userId;
                    changeView(patientsStatisticViewmodel);
                    break;
                default:
                    break;
            }
        }


        //TODO: perhaps come up with some navigation manager because right now this logic is done in 2 different entities
        private void SidebarDataContext_RequestWindowChange(object sender, Utilities.RequestWindowChangeEventArgs e)
        {
            switch (e.TargetWindow)
            {
                case View.Utilities.WindowType.GlobalStatistics:
                    changeView(globalStatisticsViewmodel);
                    break;
                case View.Utilities.WindowType.Reports:
                    changeView(reportsViewmodel);
                    break;
                case View.Utilities.WindowType.Chats:
                    changeView(chatsViewmodel);
                    break;
                case View.Utilities.WindowType.Settings:
                    changeView(settingsViewmodel);
                    break;
                case View.Utilities.WindowType.PatientStatistics:
                    changeView(patientsStatisticViewmodel);
                    break;
                case View.Utilities.WindowType.Logout:
                    //this.apiClient.Dispose();
                    //this.apiClient = new MedApiClient(Properties.Settings.Default.ApiURL);
                    RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(WindowType.Login));
                    break;
                default:
                    break;
            }
        }

        public SidebarViewmodel SidebarDataContext 
        {
            get => this.sidebarDataContext;
            set
            {
                this.sidebarDataContext = value;
                updateProperty(nameof(SidebarDataContext));
            } 
        }
        public BaseViewmodel SubContent
        {
            get => subContent;
            set
            {
                if (this.subContent != value)
                {
                    this.subContent = value;
                    this.updateProperty(nameof(SubContent));
                }
            }
        }

        private void changeView(BaseViewmodel nextView)
        {
            this.SubContent?.Close();
            nextView.Open();
            this.SubContent = nextView;
        }
    }
}
