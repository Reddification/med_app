﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MedApp.ApiClient
{
    public class AuthenticationResult
    {
        public AuthenticationResult(bool isSuccess, string serverMessage, HttpStatusCode serverResponse)
        {
            IsSuccess = isSuccess;
            ServerMessage = serverMessage;
            ServerResponse = serverResponse;
        }

        public bool IsSuccess { get; }
        public string ServerMessage { get; }
        public HttpStatusCode ServerResponse { get; }
    }
}
