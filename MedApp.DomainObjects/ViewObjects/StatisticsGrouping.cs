﻿namespace MedApp.DomainObjects.ViewObjects
{
    public enum StatisticsGrouping
    {
        Country,
        Region,
        City
    }
}
