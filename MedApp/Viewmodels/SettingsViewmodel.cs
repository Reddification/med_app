﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using MedApp.ApiClient;
using MedApp.View.Utilities;

namespace MedApp.View.Viewmodels
{
    public class SettingsViewmodel : BaseViewmodel
    {
        private string newPassword;
        private string newPassword2;
        private string oldPassword;


        public SettingsViewmodel(IApiClient ac) : base(ac)
        {
            ChangePasswordCommand = new LambdaCommand<object>(changePassword, (c) => true);
        }

        public string NewPassword 
        { 
            get => newPassword;
            set 
            {
                this.newPassword = value;
                updateProperty(nameof(NewPassword));
            }
        }

        public string NewPassword2 
        {
            get => newPassword2;
            set
            {
                this.newPassword2 = value;
                updateProperty(nameof(NewPassword2));
            }
        }

        public string OldPassword 
        {
            get => oldPassword;
            set
            {
                this.oldPassword = value;
                updateProperty(nameof(OldPassword));
            }
        }

        public ICommand ChangePasswordCommand { get; set; }
        void changePassword(object arg)
        {
            string response = this.apiClient.ChangePassword(OldPassword, NewPassword, NewPassword2, out HttpStatusCode hsc);
            if ((int)hsc >= 200 && (int)hsc < 300)
                MessageBox.Show("Password changed successfully", "Password change", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else MessageBox.Show("Failed to change password. " + response, "Password change", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
