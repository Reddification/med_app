﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedApp.View.Utilities
{
    public enum WindowType
    {
        GlobalStatistics,
        Reports,
        Chats,
        Settings,
        Login,
        Sidebar,
        Main,
        Register,
        PatientStatistics,
        Logout
    }
    public delegate void RequestWindowChangeEventHandler(object sender, RequestWindowChangeEventArgs e);
    public class RequestWindowChangeEventArgs
    {
        public WindowType TargetWindow { get; private set; }
        public object Parameter { get; set; }
        public RequestWindowChangeEventArgs(WindowType nextWindow, object parameter = null)
        {
            this.TargetWindow = nextWindow;
            this.Parameter = parameter;
        }
    }
}
