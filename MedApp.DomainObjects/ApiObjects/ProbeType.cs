﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ApiObjects
{
    // /core/probe_types
    public class ProbeType:IComparable
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("input_method")]
        public int InputMethod { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is ProbeType))
                return -1;

            if ((obj as ProbeType).Id == this.Id)
                return 0;
            else return (obj as ProbeType).Id < this.Id ? 1 : -1;
        }
    }
}
