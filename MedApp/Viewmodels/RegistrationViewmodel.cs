﻿using MedApp.ApiClient;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MedApp.View.Viewmodels
{
    public class RegistrationViewmodel : BaseViewmodel, IViewChanger
    {
        private string login;
        public RegistrationViewmodel(IApiClient ac) : base(ac)
        {
            this.RegisterCommand = new LambdaCommand<object>(Register, ((o) => true));
        }

        public event RequestWindowChangeEventHandler RequestWindowChange;

        public string Login
        {
            get => login;
            set
            {
                this.login = value;
                updateProperty(nameof(Login));
            }
        }

        public ICommand RegisterCommand { get; set; }

        private void Register(object pwdBox)
        {
            //apiClient.Register();
        } 
    }
}
