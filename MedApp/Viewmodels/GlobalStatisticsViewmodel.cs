﻿using LiveCharts;
using LiveCharts.Wpf;
using MedApp.ApiClient;
using MedApp.ApiClient.RequestParameters;
using MedApp.DomainObjects;
using MedApp.DomainObjects.ViewObjects;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace MedApp.View.Viewmodels
{
    public class GlobalStatisticsViewmodel : DataLoadingViewmodel<MedProbe>
    {
        public DataTable StatisticData = new DataTable();

        public DataTable ChartDataSource = new DataTable();

        //public SeriesCollection SeriesCollection { get; set; }
        //public string[] Labels { get; set; }
        //public Func<double, string> YFormatter { get; set; }

        private SeriesCollection seriescollection;
        public SeriesCollection SeriesCollection
        {
            get => seriescollection;
            set
            {
                this.seriescollection = value;
                updateProperty(nameof(seriescollection));
            }
        }

        private string[] labels;
        public string[] Labels
        {
            get => labels;
            set
            {
                this.labels = value;
                updateProperty(nameof(labels));
            }
        }
        private Func<string, string> yformatter;
        public Func<string, string> YFormatter
        {
            get => yformatter;
            set
            {
                this.yformatter = value;
                updateProperty(nameof(yformatter));
            }
        }

        private string probe;
        public string Probe
        {
            get => probe;
            set
            {
                this.probe = value;
                updateProperty(nameof(probe));
            }
        }

        private int minage;
        public int MinAge
        {
            get => minage;
            set
            {
                this.minage = value;
                updateProperty(nameof(minage));
            }
        }

        private int maxage;
        public int MaxAge
        {
            get => maxage;
            set
            {
                this.maxage = value;
                updateProperty(nameof(maxage));
            }
        }

        private string gender;
        public string Gender
        {
            get => gender;
            set
            {
                this.gender = value;
                updateProperty(nameof(gender));
            }
        }

        private string tbcount;

        public string TbCount
        {
            get => tbcount;
            set
            {
                this.tbcount = value;
                updateProperty(nameof(tbcount));
            }
        }

        private string tbmin;
        public string TbMin
        {
            get => tbmin;
            set
            {
                this.tbmin = value;
                updateProperty(nameof(tbmin));
            }
        }

        private string tbmax;
        public string TbMax
        {
            get => tbmax;
            set
            {
                this.tbmax = value;
                updateProperty(nameof(tbmax));
            }
        }

        private string tbaverage;
        public string TbAverage
        {
            get => tbaverage;
            set
            {
                this.tbaverage = value;
                updateProperty(nameof(tbaverage));
            }
        }



        private List<string> probelist = new List<string>() {"Weight (kg)",
                                                        "Height (cm)",
                                                        "Calories eaten",
                                                        "Calories burn",
                                                        "Waist (cm)",
                                                        "Walking time (min)",
                                                        "Exercies time (min)"};

        private List<string> genderlist = new List<string>() { "All", "Male", "Female" };

        private int[] age = Enumerable.Range(0, 85).ToArray();

        public List<string> ProbeList { get => probelist; }
        public List<string> GenderList { get => genderlist; }
        public int[] Age { get => age; }
        public int[] MaxAgeList { get => age; }

        private StatisticsGrouping groupingFilter { get; set; }
        public GlobalStatisticsViewmodel(IApiClient ac) : base(ac, 2000)
        {
        }

        public StatisticsGrouping GroupingFilter
        {
            get => groupingFilter;
            set
            {
                if (groupingFilter != value)
                {
                    groupingFilter = value;
                    updateProperty(nameof(GroupingFilter));
                }
            }
        }
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        public override async void ReloadData()
        {
            StatisticData.Clear();
            if (StatisticData.Columns.Count == 0)
            {
                Probe = ProbeList[0];
                MinAge = 0;
                MaxAge = 83;
                Gender = GenderList[0];

                StatisticData.Columns.Add("Probe");
                StatisticData.Columns.Add("Value");
                StatisticData.Columns.Add("Age");
                StatisticData.Columns.Add("Gender");

                ChartDataSource.Columns.Add("Value");
                ChartDataSource.Columns.Add("Age");
            }


            this.preLoad();
            var newStats = await this.apiClient.GetMedProbesAsync(false);
            List<Profile> profileList = new List<Profile>(this.apiClient.GetContactPersonList(true));
            this.postLoad(newStats);

            foreach (var row in newStats)
            {
                if (profileList.Where(c => c.UserId == row.PatientId).Count() == 1)
                {
                    StatisticData.Rows.Add(row.ProbeType, row.Value, profileList.Where(c => c.UserId == row.PatientId).FirstOrDefault().Age.ToString(), profileList.Where(c => c.UserId == row.PatientId).FirstOrDefault().Gender.ToString());
                }
            }
            
            LoadStat();
        }

        public void LoadStat()
        {
            ChartDataSource.Clear();

            for (int i = 0; i < StatisticData.Rows.Count; i++)
            {
                if (Probe.Trim().Contains(StatisticData.Rows[i]["Probe"].ToString().Trim()) && int.Parse(StatisticData.Rows[i]["Age"].ToString()) >= MinAge && int.Parse(StatisticData.Rows[i]["Age"].ToString()) <= MaxAge && (StatisticData.Rows[i]["Gender"].ToString() == Gender || Gender == "All"))
                {
                    ChartDataSource.Rows.Add(int.Parse(StatisticData.Rows[i]["Value"].ToString()), int.Parse(StatisticData.Rows[i]["Age"].ToString()));
                }
            }
            if (ChartDataSource.Rows.Count.ToString() != TbCount)
            {
                var newSort = ChartDataSource.AsEnumerable()
                        .GroupBy(row => new
                        {
                            Age = row.Field<string>("Age")
                        })
                        .Select(g => new
                        {
                            g.Key.Age,
                            Value = g.Average(row => double.Parse(row.Field<string>("Value")))
                        }).OrderBy(x => x.Age);

                if (newSort.Count() > 0)
                {
                    TbCount = ChartDataSource.Rows.Count.ToString();

                    TbMin = Math.Round(newSort.AsEnumerable().Min(g => g.Value), 2).ToString("");

                    TbMax = Math.Round(newSort.AsEnumerable().Max(g => g.Value), 2).ToString("");

                    TbAverage = Math.Round(newSort.AsEnumerable().Average(r => r.Value), 2).ToString("");
                }

                App.Current.Dispatcher.BeginInvoke((Action)delegate
                {
                    SeriesCollection = new SeriesCollection
                    {
                     new LineSeries
                     {
                        Title = Probe,
                        Values = new ChartValues<double>(newSort.Select(c=>c.Value))
                     }
                    };
                    Labels = newSort.Select(c => c.Age.ToString()).ToArray();
                    YFormatter = value => value.ToString();
                });
            }

            //App.Current.Dispatcher.BeginInvoke((Action)delegate
            //{
            //    SeriesCollection = new SeriesCollection
            //     {
            //        new LineSeries
            //        {
            //            Title = "Series 1",
            //            Values = new ChartValues<double> { 4, 6, 5, 2 ,4 }
            //        }
            //     };

            //    Labels = new[] { "Jan", "Feb", "Mar", "Apr", "May" };
            //    YFormatter = value => value.ToString("C");
            //});
        }
    }
}
