﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects
{
    public static class ServerMappingDictionary
    {
        public const string PatientRole = "Client";
        public const string ExpertRole = "Expert";
        public const string AdminRole = "Admin";
    }
}
