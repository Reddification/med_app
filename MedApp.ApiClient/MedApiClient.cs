﻿using MedApp.ApiClient.RequestParameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using MedApp.DomainObjects;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using MedApp.DomainObjects.ViewObjects;

namespace MedApp.ApiClient
{

    public sealed class MedApiClient : IApiClient, IDisposable
    {
        private string baseAddress = "";

        private HttpClient httpClient;
        private HttpClient externalResourceHttpClient;

        private Token token;
        private TaskFactory taskFactory;

        private const string profilesUrl = "core/all_profiles/";
        private const string probeTypesUrl = "core/probe_types/";
        private const string probesUrl = "core/probes/";
        private const string chatsUrl = "core/chats/";
        private const string messagesUrl = "core/messages/";
        private const string rolesUrl = "core/roles/";

        private const string tokenUrl = "api/token/";
        private const string refreshTokenUrl = "api/token/refresh/";
        private const string changePasswordUrl = "accounts/change-password/";

        private ApiCache cache;

        public MedApiClient(string serverURL)
        {
            cache = new ApiCache();
            this.baseAddress = serverURL;
            this.httpClient = new HttpClient()
            {
                //BaseAddress = new Uri(serverURL)
            };

            externalResourceHttpClient = new HttpClient();

            this.taskFactory = new TaskFactory();

        }

        ~MedApiClient()
        {
            this.Dispose();
        }


        #region IApiClient implementation

        #region authentication
        public AuthenticationResult SignIn(string login, string password)
        {
            using (StringContent requestContent = new StringContent(JsonConvert.SerializeObject(new { username = login, password = password }), Encoding.UTF8, "application/json"))
            {
                var hrm = this.httpClient.PostAsync(baseAddress.Trim('/') + "/" + tokenUrl, requestContent).Result;

                string serverResponse = hrm.Content.ReadAsStringAsync().Result;

                AuthenticationResult authRes = new AuthenticationResult(hrm.IsSuccessStatusCode, hrm.IsSuccessStatusCode ? null : serverResponse, hrm.StatusCode);
                if (hrm.IsSuccessStatusCode)
                    this.SetToken(serverResponse);
                return authRes;
            }
        }
        public bool IsAuthorized()
        {
            bool alreadyAuthorized = token != null && token.ExpirationDate > DateTime.Now;

            if (!alreadyAuthorized)
            {
                if (token != null && token.ExpirationDate < DateTime.Now)
                {
                    refreshToken();
                    return token != null;
                }
                else return false;
            }
            else return true;
        }
        #endregion

        #region med probes

        public IEnumerable<MedProbe> GetMedProbes(bool forceRefresh)
        {
            loadProbes(forceRefresh);
            return this.cache.Probes.Data;
        }

        public Task<IEnumerable<MedProbe>> GetMedProbesAsync(bool forceRefresh)
        {
            return taskFactory.StartNew(() => GetMedProbes(forceRefresh), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }

        public IEnumerable<DomainObjects.ViewObjects.MedProbe> GetMedProbes(int patientId, bool forceRefresh)
        {
            if (this.cache.Probes.Expired || forceRefresh)
                loadProbes(forceRefresh);

            return this.cache.Probes.Data.Where(w => w.PatientId == patientId);
        }

        public Task<IEnumerable<DomainObjects.ViewObjects.MedProbe>> GetMedProbesAsync(int patientId, bool forceRefresh)
            => taskFactory.StartNew(() => GetMedProbes(patientId, forceRefresh), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);

        #endregion


        public IEnumerable<DomainObjects.ViewObjects.Profile> GetContactPersonList(bool forceRefresh)
        {
            if (cache.Profiles.Expired || forceRefresh)
                getProfiles(forceRefresh);

            return cache.Profiles.Data.Where(w => w.RoleTitle == ServerMappingDictionary.PatientRole);//return patients that have chats with this expert
        }

        public Task<IEnumerable<DomainObjects.ViewObjects.Profile>> GetContactPersonListAsync(bool forceRefresh)
            => taskFactory.StartNew(() => GetContactPersonList(forceRefresh), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);

        public DomainObjects.ViewObjects.Expert GetExpertInfo(bool forceRefresh)
        {
            if (cache.Profiles.Expired || forceRefresh)
                getProfiles(forceRefresh);

            var expertProfile = cache.Profiles.Data.FirstOrDefault(f => f.UserId == token.UserId);
            return new DomainObjects.ViewObjects.Expert()
            {
                UserId = expertProfile.UserId,
                ProfileId = expertProfile.Id,
                Name = expertProfile.Name,
                ProfileImage = expertProfile.Photo
            };
        }

        #region chats
        public IEnumerable<DomainObjects.ViewObjects.ChatMessage> GetChatMessages(int chatId, bool forceRefresh)
        {
            Chat chat = cache.Chats.Data.FirstOrDefault(f => f.Id == chatId);

            var viewableChatMessages = chat.Messages.Where(w => w.Content != null && w.SenderId.HasValue);

            if (chat == null || !chat.Messages.Any() || forceRefresh || cache.Messages.Expired)
            {
                loadChats(forceRefresh);

                var chatMessages = getData<IEnumerable<DomainObjects.ApiObjects.Message>>(messagesUrl).Where(w => w.Chat == chat.Id && w.Text != null && w.Author != null)
                                                                                                      .OrderBy(ob => ob.CreatedAt);

                chat.Messages.Clear();
                var castedChatMessages = chatMessages.Select(s => new DomainObjects.ViewObjects.ChatMessage()
                {
                    Content = s.Text,
                    Id = s.Id,
                    ChatId = s.Chat,
                    SentDate = s.CreatedAt,
                    IsRead = s.IsRead,
                    SenderId = s.Author,
                    IsExpertMessage = s.Author == token.UserId
                });
                chat.Messages.AddRange(castedChatMessages);
                cache.Messages.Append(castedChatMessages);
                return castedChatMessages;
            }
            else return viewableChatMessages.OrderBy(ob => ob.SentDate);

        }

        public Task<IEnumerable<DomainObjects.ViewObjects.ChatMessage>> GetChatMessagesAsync(int chatId, bool forceRefresh)
        {
            return taskFactory.StartNew(() => { return GetChatMessages(chatId, forceRefresh); }, CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }

        public DomainObjects.ViewObjects.ChatMessage SendMessage(string content, int chatId)
        {
            Chat currentChat = cache.Chats.Data.FirstOrDefault(f => f.Id == chatId);
            var newMessage = sendData<DomainObjects.ApiObjects.Message>(messagesUrl, new DomainObjects.ApiObjects.Message()
            {
                Author = token.UserId,
                Chat = currentChat.Id,
                Text = content,
                CreatedAt = DateTime.Now
            });

            if (newMessage == null)
                return null;
            else
            {
                var newViewMessage = new DomainObjects.ViewObjects.ChatMessage()
                {
                    ChatId = currentChat.Id,
                    Content = content,
                    Id = newMessage.Id,
                    SentDate = newMessage.CreatedAt,
                    SenderId = newMessage.Author.HasValue ? newMessage.Author.Value : token.UserId,
                    IsExpertMessage = true
                };
                currentChat.Messages.Add(newViewMessage);
                cache.Messages.Data.Add(newViewMessage);
                return newViewMessage;
            }
        }

        public Task<DomainObjects.ViewObjects.ChatMessage> SendMessageAsync(string content, int chatId)
        {
            return this.taskFactory.StartNew(() => SendMessage(content, chatId), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }


        public Task<IEnumerable<ChatMessage>> GetNewMessagesAsync(int chatId, int lastMessageId)
        {
            return taskFactory.StartNew(() => GetNewMessages(chatId, lastMessageId), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }

        public IEnumerable<DomainObjects.ViewObjects.ChatMessage> GetNewMessages(int chatId, int lastMessageId)
        {
            Chat chat = cache.Chats.Data.FirstOrDefault(f => f.Id == chatId);
            IEnumerable<DomainObjects.ViewObjects.ChatMessage> castedChatMessages = new ChatMessage[] { };
            if (chat != null)
            {
                //loadChats(true);
                var chatMessages = getData<IEnumerable<DomainObjects.ApiObjects.Message>>(messagesUrl)
                                   .Where(w => w.Chat == chat.Id && w.Text != null && w.Author != null && w.Id > lastMessageId)
                                   .OrderBy(ob => ob.CreatedAt);

                if (chatMessages?.Any() ?? false)
                {
                    castedChatMessages = chatMessages.Select(s => new DomainObjects.ViewObjects.ChatMessage()
                    {
                        Content = s.Text,
                        Id = s.Id,
                        ChatId = s.Chat,
                        SentDate = s.CreatedAt,
                        IsRead = s.IsRead,
                        SenderId = s.Author,
                        IsExpertMessage = s.Author == token.UserId
                    });

                    chat.HasUnreadMessages = true;
                    chat.Messages.AddRange(castedChatMessages);
                }
            }
            return castedChatMessages;
        }

        public List<Chat> GetChats(bool forceRefresh)
        {
            if (cache.Chats.Expired || forceRefresh)
                loadChats(forceRefresh);

            return cache.Chats.Data;
        }

        public Task<List<Chat>> GetChatsAsync(bool forceRefresh)
        {
            return taskFactory.StartNew(() => GetChats(forceRefresh), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }
        #endregion

        #endregion


        #region token stuff

        private void SetToken(string serverToken)
        {
            this.token = JsonConvert.DeserializeObject<Token>(serverToken);
            this.httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.Access);
        }


        private void refreshToken()
        {
            using (StringContent requestContent = new StringContent(JsonConvert.SerializeObject(new { Refresh = this.token.Refresh })))
            {
                var hrm = httpClient.PostAsync(baseAddress.Trim('/') + "/" + refreshTokenUrl, requestContent).Result;
                if (hrm.IsSuccessStatusCode)
                    SetToken(hrm.Content.ReadAsStringAsync().Result);
                else this.token = null;
            }
        }
        #endregion


        public void Dispose()
        {
            this.httpClient.CancelPendingRequests();
            this.externalResourceHttpClient.CancelPendingRequests();
            this.httpClient.Dispose();
            this.externalResourceHttpClient.Dispose();
        }


        #region aux data loaders
        private IEnumerable<DomainObjects.ViewObjects.Profile> getProfiles(bool forceRefresh)
        {
            if (this.cache.Profiles.Expired || forceRefresh)
            {
                getRoles(forceRefresh);
                var apiProfiles = getData<List<DomainObjects.ApiObjects.Profile>>(profilesUrl);
                cache.Profiles.Refill(apiProfiles.Select(s => new DomainObjects.ViewObjects.Profile
                {
                    Id = s.Id,
                    Name = s.FirstName + " " + s.LastName,
                    Gender = s.Gender == "F" ? Gender.Female : Gender.Male,
                    Description = s.Description,
                    Status = s.Status,
                    Photo = s.Img != null ? getImage(s.Img) : null,
                    RoleTitle = cache.Roles.Data.FirstOrDefault(f => f.Id == s.Role).Name,
                    RoleId = s.Role,
                    UserId = s.User,
                    DateOfBirth = s.DateOfBirth
                }));
            }

            return cache.Profiles.Data;
        }

        private byte[] getImage(string img)
        {
            try
            {
                if (img.StartsWith("data:image", StringComparison.OrdinalIgnoreCase) && img.IndexOf("base64", StringComparison.OrdinalIgnoreCase) > 0)
                    return Convert.FromBase64String(img.Substring(img.IndexOf("base64,") + "base64,".Length));
                else return externalResourceHttpClient.GetAsync(img).Result.Content.ReadAsByteArrayAsync().Result;
            }
            catch (Exception e)
            {
            }
            return null;
        }

        private void loadChats(bool forceRefresh)
        {
            var profiles = getProfiles(forceRefresh);
            var expertInfo = GetExpertInfo(false);

            var chats = this.getData<IEnumerable<DomainObjects.ApiObjects.Chat>>(chatsUrl);
            this.cache.Chats.Refill(chats.Select(s => new DomainObjects.ViewObjects.Chat()
            {
                Id = s.Id,
                Members = s.Members,
                Messages = new List<DomainObjects.ViewObjects.ChatMessage>(),
                CreatedAt = s.CreatedAt,
                ChatIcon = profiles.FirstOrDefault(f => s.Members.Except(new int[] { expertInfo.UserId }).Contains(f.UserId))?.Photo,
                ChatTitle = string.Join(", ", profiles.Where(w => s.Members.Except(new int[] { expertInfo.UserId }).Contains(w.UserId)).Select(ss => ss.Name)),
                HasUnreadMessages = false,
                Participants = profiles.Where(w => s.Members.Contains(w.UserId)).ToList()
            }));
        }

        private void getRoles(bool forceRefresh)
        {
            if (cache.Roles.Expired || forceRefresh)
                this.cache.Roles.Refill(getData<IEnumerable<DomainObjects.ApiObjects.Role>>(rolesUrl));
        }

        private T getData<T>(string resource, string parameters = null)
        {
            if (parameters != null)
                resource += "/" + parameters;

            var hrm = httpClient.GetAsync(baseAddress.Trim('/') + "/" + resource).Result;
            if (hrm.IsSuccessStatusCode)
            {
                string contentResponse = hrm.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(contentResponse);
            }
            else return default(T);
        }

        private Task<T> getDataAsync<T>(string resource, string parameters = null)
        {
            return taskFactory.StartNew(() => getData<T>(resource, parameters), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }

        private T sendData<T>(string resource, object data)
        {
            T result = default(T);
            using (StringContent requestContent = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"))
            {
                HttpResponseMessage hrm = httpClient.PostAsync(baseAddress.Trim('/') + "/" + resource, requestContent).Result;
                if (hrm.IsSuccessStatusCode)
                    result = JsonConvert.DeserializeObject<T>(hrm.Content.ReadAsStringAsync().Result);
                return result;
            }
        }

        private Task<T> sendDataAsync<T>(string resource, object data)
        {
            return taskFactory.StartNew(() => sendData<T>(resource, data), CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }

        private void loadProbes(bool forceRefresh)
        {
            if (cache.ProbeTypes.Expired || forceRefresh)
                cache.ProbeTypes.Refill(getData<IEnumerable<DomainObjects.ApiObjects.ProbeType>>(probeTypesUrl));

            if (cache.Probes.Expired || forceRefresh)
            {
                var medProbes = getData<IEnumerable<DomainObjects.ApiObjects.Probe>>(probesUrl);
                this.cache.Probes.Refill(medProbes.Select(s => new DomainObjects.ViewObjects.MedProbe()
                {
                    Captured = s.CreatedAt,
                    Id = s.Id,
                    PatientId = s.User,
                    ProbeType = s.ProbeTypeName,
                    ProbeTypeId = s.ProbeType,
                    Value = s.Value
                }));
            }
        }
        #endregion

        private int mapProfileIdToUserId(int profileId)
        {
            if (cache.Profiles.Expired)
                getProfiles(false);
            return cache.Profiles.Data.FirstOrDefault(f => f.Id == profileId).UserId;
        }
        public void UpdateServerAddress(string newAddress)
        {
            this.baseAddress = newAddress;
        }

        public ChangePasswordResponse ChangePassword(string oldPassword, string newPassword, string newPasswordConfirmation)
        {
            return this.sendData<ChangePasswordResponse>(changePasswordUrl, new ChangePasswordRequest()
            {
                NewPassword = newPassword,
                NewPasswordRepeat = newPasswordConfirmation,
                OldPassword = oldPassword
            });
        }

        public string ChangePassword(string oldPassword, string newPassword, string newPasswordConfirmation, out HttpStatusCode hsc)
        {
            var cpr = new ChangePasswordRequest()
            {
                NewPassword = newPassword,
                NewPasswordRepeat = newPasswordConfirmation,
                OldPassword = oldPassword
            };

            using (StringContent requestContent = new StringContent(JsonConvert.SerializeObject(cpr), Encoding.UTF8, "application/json"))
            {
                HttpResponseMessage hrm = httpClient.PostAsync(baseAddress.Trim('/') + "/" + changePasswordUrl, requestContent).Result;
                hsc = hrm.StatusCode;
                return hrm.Content.ReadAsStringAsync().Result;
            }
        }

        public Chat CreateChat(int expertId, int userId)
        {
            DomainObjects.ApiObjects.Chat newChat = sendData<DomainObjects.ApiObjects.Chat>(chatsUrl, new DomainObjects.ApiObjects.Chat()
            {
                Members = new int[] { expertId, userId },
                CreatedAt = DateTime.Now
            });

            Profile userProfile = cache.Profiles.Data.FirstOrDefault(f => f.UserId == userId);

            Chat mappedChat = new Chat()
            {
                Id = newChat.Id,
                CreatedAt = newChat.CreatedAt,
                Members = newChat.Members,
                ChatIcon = userProfile?.Photo,
                ChatTitle = userProfile.Name,
                Participants = new List<Profile>() { userProfile, cache.Profiles.Data.FirstOrDefault(f => f.UserId == expertId) },
                HasUnreadMessages = false,
                Messages = new List<ChatMessage>()
            };

            cache.Chats.Data.Add(mappedChat);

            return mappedChat;
        }
    }
}
