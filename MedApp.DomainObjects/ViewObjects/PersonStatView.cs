﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    class PersonStatView:IComparable
    {
        public int Id { get; set; }
        public int CompareTo(object obj)
        {
            if (!(obj is PersonStatView))
                return -1;

            if ((obj as PersonStatView).Id == this.Id)
                return 0;
            else return (obj as PersonStatView).Id < this.Id ? 1 : -1;
        }
    }
}
