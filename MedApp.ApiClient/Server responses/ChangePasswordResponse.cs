﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient
{
    public class ChangePasswordResponse
    {
        public bool IsSuccess { get; set; }
        public string ServerMessage { get; set; }
    }
}
