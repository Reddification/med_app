﻿using MedApp.DomainObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.RequestParameters
{
    

    public class StatisticsRequestParameters
    {
        public int ParameterId { get; set; }
        public int RegionId { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public Gender Gender { get; set; }
    }
}
