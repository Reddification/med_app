﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    public class Chat:IComparable
    {
        public int Id { get; set; }
        public int[] Members { get; set; }
        public List<ChatMessage> Messages { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool HasUnreadMessages { get; set; }

        public byte[] ChatIcon { get; set; }
        public string ChatTitle { get; set; }
        public List<Profile> Participants { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is Chat))
                return -1;

            if ((obj as Chat).Id == this.Id)
                return 0;
            else return (obj as Chat).Id < this.Id ? 1 : -1;
        }
    }
}
