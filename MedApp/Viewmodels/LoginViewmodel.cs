﻿using MedApp.ApiClient;
using MedApp.View.Utilities;
using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MedApp.View.Viewmodels
{
    public class LoginViewmodel : BaseViewmodel, IViewChanger
    {
        private bool debug = true;
        private string login;
        private bool rememberMe;
        private string errorMessage = "";
        private string serverAddress = "";
        private LoginStatus status;
        private SettingsManager settingsManager;

        private ICommand signInCommand;
        public LoginViewmodel(IApiClient apiClient) : base(apiClient)
        {
            this.signInCommand = new LambdaCommand<object>(this.SignIn, c => canSignIn(c));
            this.RegistrationCommand = new LambdaCommand<object>(this.Register, p => true);
            status = LoginStatus.Idle;
            settingsManager = new SettingsManager();
            if (settingsManager.NeedReminders())
            {
                this.RememberMe = true;
                this.Login = settingsManager.RemindUsername();
                this.ServerAddress = settingsManager.RemindServerAddress();
            }
            //TODO: read saved login
        }

        public string Login
        {
            get => login;
            set
            {
                this.login = value;
                updateProperty(nameof(Login));
            }
        }
        public bool RememberMe
        {
            get => rememberMe; 
            set
            {
                rememberMe = value;
                updateProperty(nameof(RememberMe));
            }
        }

        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage != value)
                {
                    errorMessage = value;
                    updateProperty(nameof(ErrorMessage));
                }
            }
        }

        internal LoginStatus Status
        {
            get => status;
            set
            {
                if (status != value)
                {
                    status = value;
                    updateProperty(nameof(Status));
                }
            }
        }

        public ICommand RegistrationCommand { get; set; }
        public ICommand SignInCommand { get => signInCommand; set => signInCommand = value; }
        public string ServerAddress 
        { 
            get => serverAddress;
            set
            {
                serverAddress = value;
                updateProperty(serverAddress);
            }
        }

        public event RequestWindowChangeEventHandler RequestWindowChange;

        public void SignIn(object pwdBox)
        {
            bool needUpdates = settingsManager.SaveReminders(RememberMe);
            if (RememberMe)
            {
                needUpdates = settingsManager.SaveServerAddress(serverAddress);
                needUpdates = settingsManager.SaveUsername(Login);
            }

            if (needUpdates)
                settingsManager.CommitSettings();

            RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(WindowType.Main));
        }

        private void Register(object args)
        {
            RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(WindowType.Register));
        }

        private bool canSignIn(object pwdBox)
        {
            //https://docs.microsoft.com/en-us/dotnet/api/system.windows.controls.passwordbox.securepassword?view=netframework-4.8
            //https://stackoverflow.com/questions/15390727/passwordbox-and-mvvm/15391318#15391318
            //https://docs.microsoft.com/en-us/dotnet/api/system.io.unmanagedmemorystream?view=netframework-4.8
            PasswordBox pb = (PasswordBox)pwdBox;//hey fuck mvvm right
            if (!string.IsNullOrEmpty(Login) && pb.SecurePassword.Length > 0)
            {
                this.apiClient.UpdateServerAddress(this.ServerAddress);

                using (var hashing = SHA512.Create())
                {
                    byte[] hashedPassword = null;
                    unsafe
                    {
                        IntPtr spBytes = Marshal.SecureStringToGlobalAllocAnsi(pb.SecurePassword);
                        //https://docs.microsoft.com/en-us/dotnet/api/system.io.unmanagedmemorystream?view=netframework-4.8
                        byte* spBytesPtr = (byte*)spBytes.ToPointer();//pointer to data in unicode
                        UnmanagedMemoryStream unmanagedStream = new UnmanagedMemoryStream(spBytesPtr, pb.SecurePassword.Length, pb.SecurePassword.Length, FileAccess.Read);

                        hashedPassword = hashing.ComputeHash(unmanagedStream);
                        unmanagedStream.Close();//it's said that disposing is not required
                        unmanagedStream.Dispose();
                        Marshal.ZeroFreeGlobalAllocAnsi(spBytes);
                    }

                    var authenticationResult = this.apiClient.SignIn(Login, debug ? pb.Password : BitConverter.ToString(hashedPassword).Replace("-",""));
                    if (authenticationResult.IsSuccess)
                        return true;
                    else switch (authenticationResult.ServerResponse)
                    {
                        case HttpStatusCode.Unauthorized:
                            ErrorMessage = "Invalid login/password";
                            loginFailedNotification(authenticationResult.ServerResponse, ErrorMessage);
                            this.Status = LoginStatus.BadCredentials;
                            break;
                        case HttpStatusCode.InternalServerError:
                        case HttpStatusCode.BadRequest:
                        case HttpStatusCode.BadGateway:
                            ErrorMessage = authenticationResult.ServerMessage;
                            loginFailedNotification(authenticationResult.ServerResponse, ErrorMessage);
                            this.Status = LoginStatus.BadServerResponse;
                                break;
                        default: break;
                    }
                    return false;
                }
            }
            else return true;
        }

        private void loginFailedNotification(HttpStatusCode hsc, string message)
        {
            MessageBox.Show($"Failed to login. Server responded with {hsc}: {message}", "SHIT SHIT FUCK", MessageBoxButton.YesNo, MessageBoxImage.Error);
        }
    }

    enum LoginStatus
    {
        Idle,
        Success,
        BadCredentials,
        BadServerResponse,
    }
}
