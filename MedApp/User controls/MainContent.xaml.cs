﻿using System.Windows.Controls;

namespace MedApp.View
{
    /// <summary>
    /// Interaction logic for MainPageContent.xaml
    /// </summary>
    public partial class MainContent : UserControl
    {
        public MainContent()
        {
            InitializeComponent();
        }
    }
}
