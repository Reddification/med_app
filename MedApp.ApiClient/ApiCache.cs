﻿using MedApp.ApiClient.Cache;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace MedApp.ApiClient
{
    class ApiCache
    {
        public ApiCache()
        {
            this.Chats = new CachedChats(TimeSpan.FromSeconds(5));
            this.Messages = new CachedMessages(TimeSpan.FromSeconds(1));
            this.Profiles = new CachedProfiles(TimeSpan.FromSeconds(10));
            this.Probes = new CachedProbes(TimeSpan.FromSeconds(30));
            this.Statistics = new CachedStatistics(TimeSpan.FromSeconds(30));
            this.Roles = new CachedRoles(TimeSpan.FromMinutes(30));
            this.ProbeTypes = new CachedProbeTypes(TimeSpan.FromMinutes(30));
        }

        public CachedChats Chats { get; private set; }
        public CachedMessages Messages { get; private set; }
        public CachedProbes Probes { get; private set; }
        public CachedProfiles Profiles { get; private set; }
        public CachedStatistics Statistics { get; private set; }
        public CachedRoles Roles { get; private set; }
        public CachedProbeTypes ProbeTypes { get; internal set; }
    }
}
