﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    public class MedProbe:IComparable
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int ProbeTypeId { get; set; }
        public string ProbeType { get; set; }
        public int Value { get; set; }
        public DateTime Captured { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is MedProbe))
                return -1;

            if ((obj as MedProbe).Id == this.Id)
                return 0;
            else return (obj as MedProbe).Id < this.Id ? 1 : -1;
        }
    }
}
