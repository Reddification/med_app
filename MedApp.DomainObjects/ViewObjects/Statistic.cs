﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    public class Statistic:IComparable
    {
        //TODO: add properties
        public int Id { get; set; }
        public int PatiendId { get; set; }
        public int Data { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is Statistic))
                return -1;

            if ((obj as Statistic).Id == this.Id)
                return 0;
            else return (obj as Statistic).Id < this.Id ? 1 : -1;

        }
    }
}
