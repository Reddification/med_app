﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    public class Recommendation
    {
        public string Content { get; set; }
    }
}
