﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    public class Expert
    {
        public int UserId { get; set; }
        public int ProfileId { get; set; }
        public string Name { get; set; }
        public byte[] ProfileImage { get; set; }
    }
}
