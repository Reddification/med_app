﻿using MedApp.DomainObjects.ViewObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient
{
    public class DataProvider
    {
        private List<Chat> chats;
        private List<ChatMessage> messages;
        private List<Expert> experts;
        private List<MedProbe> probes;
        private List<Profile> profiles;
        private List<Statistic> statistics;

        public DataProvider(IApiClient apiClient)
        {

        }

        public List<Chat> Chats { get => chats; set => chats = value; }
        public List<ChatMessage> Messages { get => messages; set => messages = value; }
        public List<Expert> Experts { get => experts; set => experts = value; }
        public List<MedProbe> Probes { get => probes; set => probes = value; }
        public List<Profile> Profiles { get => profiles; set => profiles = value; }
        public List<Statistic> Statistics { get => statistics; set => statistics = value; }
    }
}
