﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ViewObjects
{
    public class ChatMessage:IComparable
    {
        public int Id { get; set; }
        public int? ChatId { get; set; }//probably redundant
        public int? SenderId { get; set; }//user id!!!
        public DateTime SentDate { get; set; }
        public string Sent { get => SentDate.ToString(); }
        public string Content { get; set; }
        public bool IsRead { get; set; }
        public bool IsExpertMessage { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is ChatMessage))
                return -1;

            if ((obj as ChatMessage).Id == this.Id)
                return 0;
            else return (obj as ChatMessage).Id < this.Id ? 1 : -1;
        }
    }
}
