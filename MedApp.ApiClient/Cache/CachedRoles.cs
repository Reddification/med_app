﻿using MedApp.DomainObjects.ApiObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public class CachedRoles : CachedData<Role>
    {
        public CachedRoles(TimeSpan lifetime) : base(lifetime)
        {
        }
    }
}
