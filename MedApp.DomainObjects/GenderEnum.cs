﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects
{
    public enum Gender
    {
        Male,
        Female,
        Diverse,
        AttackHelicopterApache,
        LegoBlock
    }
}
