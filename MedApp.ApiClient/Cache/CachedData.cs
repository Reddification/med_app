﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public abstract class CachedData<T> where T:IComparable
    {
        private readonly TimeSpan lifetime;
        private List<T> data;
        private DateTime expires;
        public CachedData(TimeSpan lifetime)
        {
            expires = DateTime.Now + lifetime;
            data = new List<T>();
            this.lifetime = lifetime;
        }

        public List<T> Data
        {
            get { return data; }
            set
            {
                this.data = value;
                Reset();
            }
        }

        public void Reset()
        {
            this.expires = DateTime.Now + lifetime;
        }

        public void Refill(IEnumerable<T> newData)
        {
            Append(newData);
        }

        public void Append(IEnumerable<T> newData)
        {
            if (newData is null)
                return;

            foreach (var t in newData)
                if (!data.Any(a => a.CompareTo(t) == 0))
                    data.Add(t);

            Reset();
        }

        public bool Expired => DateTime.Now > expires;
    }
}
