﻿using MedApp.ApiClient;
using MedApp.DomainObjects;
using Microsoft.Win32;
using System.ComponentModel;
using System.Windows.Forms;

namespace MedApp.View.Viewmodels
{
    public abstract class BaseViewmodel : INotifyPropertyChanged
    {
        protected IApiClient apiClient;
        public BaseViewmodel(IApiClient ac)
        {
            this.apiClient = ac;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void updateProperty(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public virtual void Open()
        {

        }

        public virtual void Close()
        {

        }
    }
}
