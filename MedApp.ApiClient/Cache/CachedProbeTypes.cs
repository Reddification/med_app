﻿using MedApp.DomainObjects.ApiObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    class CachedProbeTypes : CachedData<ProbeType>
    {
        public CachedProbeTypes(TimeSpan lifetime) : base(lifetime)
        {
        }
    }
}
