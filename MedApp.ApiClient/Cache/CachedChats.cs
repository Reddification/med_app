﻿using MedApp.DomainObjects.ViewObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public class CachedChats : CachedData<Chat>
    {
        public CachedChats(TimeSpan lifetime):base(lifetime)
        {
        }
    }
}
