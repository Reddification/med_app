﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MedApp.ApiClient.RequestParameters;
using MedApp.DomainObjects;

namespace MedApp.ApiClient
{
    //public class TestApiClient : IApiClient
    //{
    //    private Random r;

    //    private List<Statistic> testStatisticsData;
    //    private List<Chat> testChats;
    //    private List<MedProbe> testMedProbes;
    //    private List<Recommendation> testRecommendations;
    //    private List<Patient> testPatients;
    //    private Expert testExpert;
    //    private TaskFactory taskFactory;
    //    private const string wordsPool = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        
    //    private void populateTestData()
    //    {
    //        this.taskFactory = new TaskFactory(TaskScheduler.Default);
    //        testExpert = new Expert() { Id = 0, Name = "Test Expert" };
    //        string[] words = wordsPool.Split(' ');
    //        for (int i = 1000; i < 1100; i++)
    //        {
    //            testMedProbes.Add(new MedProbe()
    //            {
    //                WaterConsumed = r.Next(1000, 4000),
    //                CaloriesConsumed = r.Next(1000, 2000),
    //                Captured = DateTime.Now,
    //                ExcerciseDuration = TimeSpan.FromMinutes(r.Next(20, 120)),
    //                SleepTime = TimeSpan.FromHours(r.Next(4, 10)),
    //                StepsDone = r.Next(100, 2300)
    //            });
    //            testStatisticsData.Add(new Statistic() { Data = r.Next(0, 100010) });
    //            testRecommendations.Add(new Recommendation() { Content = words[r.Next(words.Length)] });

    //            //testChats[r.Next(testChats.Count)].Messages.Add(new ChatMessage() { Sent = DateTime.Now, Id = i, Content = wordsPool.Substring(p, r.Next(wordsPool.Length - p - 1)) });
    //        }

    //        int usersCount = r.Next(10, 20);
    //        for (int i = 1; i <= usersCount; i++)
    //        {
    //            Patient newPatient = new Patient()
    //            {
    //                Id = i,
    //                Name = words[r.Next(words.Length)] + " " + words[r.Next(words.Length)],
    //                HasUnreadMessages = i % 3 == 0
    //            };

    //            testPatients.Add(newPatient);

    //            Chat newChat = new Chat() { Id = 100 + i, Messages = new List<ChatMessage>(), Patient = newPatient};

    //            int messagesCount = r.Next(5, 15);

    //            for (int j = 0; j < messagesCount; j++)
    //            {   
    //                int p = r.Next(wordsPool.Length - 10);
    //                newChat.Messages.Add(new ChatMessage()
    //                {
    //                    Content = wordsPool.Substring(p, r.Next(wordsPool.Length - p - 1)),
    //                    Id = r.Next(),
    //                    SenderId = j % 3 == 0 ? testExpert.Id : newPatient.Id,
    //                    SentDate = DateTime.Now - TimeSpan.FromHours(r.Next(1, 20))
    //                });
    //            }

    //            testChats.Add(newChat);
    //        }

    //    }

    //    public void Dispose()
    //    {
    //    }

    //    public TestApiClient()
    //    {
    //        r = new Random((int)DateTime.Now.Ticks);
    //        testMedProbes = new List<MedProbe>();
    //        testStatisticsData = new List<Statistic>();
    //        testRecommendations = new List<Recommendation>();
    //        testChats = new List<Chat>();
    //        testPatients = new List<Patient>();
    //        populateTestData();
    //    }

    //    public IEnumerable<MedProbe> GetMedProbes()
    //    {
    //        Thread.Sleep(r.Next(500, 1000));
    //        return this.testMedProbes;
    //    }

    //    public Task<IEnumerable<MedProbe>> GetMedProbesAsync() => Task.Run(() => GetMedProbes());

    //    //public IEnumerable<Recommendation> GetRecommendations()
    //    //{
    //    //    Thread.Sleep(r.Next(500, 1500));
    //    //    return testRecommendations;
    //    //}

    //    //public Task<IEnumerable<Recommendation>> GetRecommendationsAsync()
    //    //{
    //    //    return Task.Run(() => GetRecommendations());
    //    //}

    //    public IEnumerable<Statistic> GetStatistics(StatisticsRequestParameters args)
    //    {
    //        Thread.Sleep(r.Next(500, 1500));
    //        return testStatisticsData;
    //    }

    //    public Task<IEnumerable<Statistic>> GetStatisticsAsync(StatisticsRequestParameters args)
    //    {
    //        return Task.Run(() => GetStatistics(args));
    //    }

    //    public bool IsAuthorized()
    //    {
    //        return true;
    //    }


    //    public AuthenticationResult SignIn(string login, string passwordHash)
    //    {
    //        string referrentPwdHash = "";
    //        using (SHA512 sha512 = SHA512.Create())
    //        {
    //            referrentPwdHash = BitConverter.ToString(sha512.ComputeHash(Encoding.UTF8.GetBytes("password"))).Replace("-", "");
    //        }
    //        bool success = login == "login" && passwordHash == referrentPwdHash;
    //        AuthenticationResult res = new AuthenticationResult(success, success? null : "Invalid login and/or password", success? HttpStatusCode.OK : HttpStatusCode.Forbidden);
    //        return res;
    //    }

    //    public List<Patient> GetContactPersonList() => testPatients;


    //    public IEnumerable<Patient> GetPatientsList() => testPatients;

    //    public Task<IEnumerable<Patient>> GetPatientsListAsync() => taskFactory.StartNew(() => GetPatientsList());

    //    public Expert GetExpertInfo() => testExpert;

    //    public IEnumerable<ChatMessage> GetChatMessages(int patientId) => testChats.FirstOrDefault(w => w.Patient.Id == patientId).Messages;

    //    //TODO: takes very long, idk where exactly is the problem
    //    public void SendMessage(string content, int patientId)
    //    {
    //        this.testChats.FirstOrDefault(f => f.Patient.Id == patientId).Messages.Add(new ChatMessage() { Content = content, SenderId = this.testExpert.Id, SentDate = DateTime.Now });
    //    }

    //    public IEnumerable<Statistic> GetPatientStatistics(int patientId)
    //    {
    //        return testStatisticsData.Where(w => w.PatiendId == patientId);
    //    }

    //    public Task<IEnumerable<ChatMessage>> GetChatMessagesAsync(int patientId)
    //    {
    //        return taskFactory.StartNew(() => GetChatMessages(patientId));
    //    }
    //}
}
