﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.RequestParameters
{
    public class PersonalStatsRequestParameters
    {
        public int RequestorId { get; set; }
        public int PersonId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int? ChatId { get; set; } //???
    }
}
