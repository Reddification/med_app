﻿using MedApp.ApiClient;
using MedApp.DomainObjects;
using MedApp.DomainObjects.ViewObjects;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MedApp.View.Viewmodels
{
    public class ExpertChatsViewmodel : DataLoadingViewmodel<ChatMessage>, IViewChanger
    {
        private string personName;
        private int activeChatId;
        private ChatListViewmodel chatListViewmodel;
        private Expert expert;
        private string currentText;

        private Chat chat;

        public event RequestWindowChangeEventHandler RequestWindowChange;

        public ExpertChatsViewmodel(IApiClient ac, Expert e) : base(ac, 1000)
        {
            this.expert = e;
            ChatListViewmodel = new ChatListViewmodel(ac, 2000);
            ChatListViewmodel.ChatSelected += ChatListViewmodel_ChatSelected;

            this.LoadPersonStatCommand = new LambdaCommand<object>(this.LoadPersonStat, p => true);
            this.SendMessageCommand = new LambdaCommand<object>(this.SendMessage, p => true);
        }

        private void ChatListViewmodel_ChatSelected(Chat selectedChat)
        {
            PersonName = string.Join(", ", selectedChat.Participants.Where(w => w.Id != this.expert.ProfileId).Select(s => s.Name));
            this.activeChatId = selectedChat.Id;
            ReloadData();
        }

        private void LoadPersonStat(object obj)
        {
            int userId = ChatListViewmodel.SelectedChat?.Participants.FirstOrDefault(f => f.UserId != expert.UserId).UserId ?? 0;
            this.RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(WindowType.PatientStatistics, userId));
        }

        public ICommand SendMessageCommand { get; set; }
        private void SendMessage(object p)
        {
            var resultMessage = this.apiClient.SendMessage(this.CurrentText, this.activeChatId);//perhaps wait for the result
            //TODO: consider listview sorting by date
            if (resultMessage != null)
            {
                this.ViewData.Add(new ChatMessage() { Content = this.CurrentText, IsExpertMessage = true, SenderId = this.expert.UserId, SentDate = DateTime.Now });
                this.CurrentText = "";
            }
            else MessageBox.Show("Couldn't send message", "oops", MessageBoxButton.OK);
        }

        internal void OpenChatWithUser(int userId)
        {
            ChatListViewmodel.ReloadData2();
            Chat chat = ChatListViewmodel.ViewData.FirstOrDefault(f => f.Participants.Any(a => a.UserId == userId));
            //if no such chat exists - create new one
            if (chat == null)
            {
                this.ChatListViewmodel.Close();
                var newChat = apiClient.CreateChat(expert.UserId, userId);
                if (newChat != null)
                {
                    this.ChatListViewmodel.ViewData.Add(newChat);
                    this.ActiveChatId = newChat.Id;
                }
                else MessageBox.Show("Failed to open a chat with user", "shittu fakku", MessageBoxButton.OK, MessageBoxImage.Error);
                this.ChatListViewmodel.Open();
            }
            else ActiveChatId = chat.Id;
        }

        public int ActiveChatId 
        {
            get => activeChatId;
            set
            {
                this.activeChatId = value;
                updateProperty(nameof(ActiveChatId));
                this.ChatListViewmodel.SelectedChat = ChatListViewmodel.ViewData.FirstOrDefault(f => f.Id == value);
                //this.ChatListViewmodel.SelectedChat.Members.Where(w => w != expert.Id).FirstOrDefault() = value;
            }
        }

        public string PersonName 
        { 
            get => personName;
            set 
            {
                this.personName = value;
                updateProperty(nameof(PersonName));
            } 
        }

        public ICommand LoadPersonStatCommand { get; set; }
        public ChatListViewmodel ChatListViewmodel { get => chatListViewmodel; set => chatListViewmodel = value; }
        public string CurrentText 
        {
            get => currentText;
            set 
            {
                this.currentText = value;
                updateProperty(nameof(CurrentText));
            }
        }

        public override async void ReloadData()
        {
            if (ChatListViewmodel.SelectedChat == null)
                return;

            IEnumerable<ChatMessage> newMessages = null;

            preLoad();

            newMessages = await this.apiClient.GetChatMessagesAsync(ChatListViewmodel.SelectedChat.Id, false);

            if (newMessages?.Any() ?? false)
            {
                foreach (var message in newMessages)
                    if (message.SenderId == expert.UserId)
                        message.IsExpertMessage = true;

            }

            postLoad(newMessages);
        }

        public override void Open()
        {
            base.Open();
            this.ChatListViewmodel.Open();
            if (this.chat != null)
                this.chat.HasUnreadMessages = false;
        }

        public override void Close()
        {
            base.Close();
            this.ChatListViewmodel.Close();
        }
    }
}
