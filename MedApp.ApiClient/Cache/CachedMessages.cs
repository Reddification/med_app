﻿using MedApp.DomainObjects.ViewObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public class CachedMessages : CachedData<ChatMessage>
    {
        public CachedMessages(TimeSpan lifetime) : base(lifetime)
        {
        }
    }
}
