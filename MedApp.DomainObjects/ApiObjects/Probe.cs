﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ApiObjects
{
    public class Probe
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("user")]
        public int User { get; set; }
        [JsonProperty("probe_type")]
        public int ProbeType { get; set; }
        [JsonProperty("probe_type_name")]
        public string ProbeTypeName { get; set; }
        [JsonProperty("value")]
        public int Value { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
    }
}
