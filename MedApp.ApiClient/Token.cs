﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace MedApp.ApiClient
{
    public class Token
    {
        private DateTime expirationDate = DateTime.MinValue;
        private int userId = -1;
        public Token(string access, string refresh)
        {
            this.Access = access;
            this.Refresh = refresh;
            extractData();
        }

        public string Access { get; }
        public string Refresh { get; }
        public DateTime ExpirationDate 
        {
            get 
            {
                if (expirationDate == DateTime.MinValue)
                    extractData();
                return expirationDate;
            } 
        }

        public int UserId 
        {
            get 
            { 
                if (this.userId == -1)
                    extractData();
                return userId;
            } 
        }

        private void extractData()
        {
            //https://stackoverflow.com/questions/38340078/how-to-decode-jwt-token
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(Access) as JwtSecurityToken;
            this.userId = Convert.ToInt32(jsonToken.Claims.FirstOrDefault(f => f.Type == "user_id").Value);
            this.expirationDate = jsonToken.ValidTo;
        }
    }
}
