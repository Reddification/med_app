﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MedApp.View.Utilities
{
    public static class Cryptography
    {
        public static byte[] CalculateHash(byte[] data)
        {
            using (SHA512 hasher = SHA512.Create())
                return hasher.ComputeHash(data);

        }
    }
}
