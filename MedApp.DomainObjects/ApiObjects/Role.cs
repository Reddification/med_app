﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ApiObjects
{
    public class Role:IComparable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is Role))
                return -1;

            if ((obj as Role).Id == this.Id)
                return 0;
            else return (obj as Role).Id < this.Id ? 1 : -1;
        }
    }
}
