﻿using MedApp.ApiClient.RequestParameters;
using MedApp.DomainObjects;
using MedApp.DomainObjects.ViewObjects;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace MedApp.ApiClient
{
    public interface IApiClient
    {
        void Dispose();

        IEnumerable<DomainObjects.ViewObjects.MedProbe> GetMedProbes(bool forceRefresh);
        Task<IEnumerable<DomainObjects.ViewObjects.MedProbe>> GetMedProbesAsync(bool forceRefresh);

        IEnumerable<DomainObjects.ViewObjects.MedProbe> GetMedProbes(int patientId, bool forceRefresh);
        Task<IEnumerable<DomainObjects.ViewObjects.MedProbe>> GetMedProbesAsync(int patientId, bool forceRefresh);

        bool IsAuthorized();

        IEnumerable<DomainObjects.ViewObjects.Profile> GetContactPersonList(bool forceRefresh);
        Task<IEnumerable<DomainObjects.ViewObjects.Profile>> GetContactPersonListAsync(bool forceRefresh);

        DomainObjects.ViewObjects.Expert GetExpertInfo(bool forceRefresh);

        List<Chat> GetChats(bool forceRefresh);
        Task<List<Chat>> GetChatsAsync(bool forceRefresh);

        IEnumerable<DomainObjects.ViewObjects.ChatMessage> GetChatMessages(int chatId, bool forceRefresh);
        Task<IEnumerable<DomainObjects.ViewObjects.ChatMessage>> GetChatMessagesAsync(int chatId, bool forceRefresh);

        DomainObjects.ViewObjects.ChatMessage SendMessage(string content, int chatId);
        Task<DomainObjects.ViewObjects.ChatMessage> SendMessageAsync(string content, int patientId);
        string ChangePassword(string oldPassword, string newPassword, string newPassword2, out HttpStatusCode hsc);
        AuthenticationResult SignIn(string login, string password);

        void UpdateServerAddress(string newAddress);

        IEnumerable<ChatMessage> GetNewMessages(int chatId, int lastMessageId);
        Task<IEnumerable<ChatMessage>> GetNewMessagesAsync(int chatId, int lastMessageId);

        ChangePasswordResponse ChangePassword(string oldPassword, string newPassword, string newPasswordConfirmation);
        Chat CreateChat(int expertId, int userId);
    }
}
