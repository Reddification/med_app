﻿using MedApp.ApiClient;
using MedApp.View.Utilities;
using System.Windows.Input;

namespace MedApp.View.Viewmodels
{
    public class SidebarViewmodel : BaseViewmodel, IViewChanger
    {
        //private ICommand openStatisticsCommand;

        public event RequestWindowChangeEventHandler RequestWindowChange;
        public string FullName { get; set; }
        public byte[] Photo { get; set; }
        public SidebarViewmodel(IApiClient ac, string fullName, byte[] photo) : base(ac)
        {
            this.ChangeViewCommand = new LambdaCommand<WindowType>(ChangeView, CanChangeView);
            this.LogoutCommand = new LambdaCommand<object>(Logout, (p) => true);
            this.FullName = fullName;
            this.Photo = photo;
        }

        public ICommand ChangeViewCommand { get; set; }
        public ICommand LogoutCommand { get; set; }

        private void Logout(object obj)
        {
            RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(WindowType.Logout));
        }

        private void ChangeView(WindowType wt)
        {
            RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(wt));
        }

        private bool CanChangeView(WindowType wt) => apiClient.IsAuthorized();
    }
}
