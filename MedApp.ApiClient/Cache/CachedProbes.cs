﻿using MedApp.DomainObjects.ViewObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public class CachedProbes : CachedData<MedProbe>
    {
        public CachedProbes(TimeSpan lifetime) : base(lifetime)
        {
        }
    }
}
