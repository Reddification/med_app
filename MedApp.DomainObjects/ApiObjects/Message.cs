﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ApiObjects
{
    public class Message
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("chat")]
        public int? Chat { get; set; }
        [JsonProperty("author")]
        public int? Author { get; set; } //USER ID
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("is_read")]
        public bool IsRead { get; set; }
    }
}
