﻿using MedApp.DomainObjects.ViewObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public class CachedProfiles : CachedData<Profile>
    {
        public CachedProfiles(TimeSpan lifetime) : base(lifetime)
        {
        }
    }
}
