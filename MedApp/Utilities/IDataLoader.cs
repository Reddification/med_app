﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedApp.View.Utilities
{
    public enum DataLoadingStatus
    {
        NotLoaded,
        Loaded,
        Loading
    }
    interface IDataLoader
    {
        DataLoadingStatus DataStatus { get; set; }
        void ReloadData();
    }
}
