﻿using MedApp.ApiClient;
using MedApp.DomainObjects.ViewObjects;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedApp.View.Viewmodels
{
    public delegate void ChatSelectedEventHandler(Chat selectedPatient);

    public class ChatListViewmodel:DataLoadingViewmodel<Chat>
    {
        private Chat selectedChat;
        public ChatListViewmodel(IApiClient ac, int refreshTimeMs) : base(ac, refreshTimeMs)
        {
        }

        public Chat SelectedChat
        {
            get => this.selectedChat;
            set
            {
                this.selectedChat = value;
                updateProperty(nameof(SelectedChat));
                ChatSelected?.Invoke(value);
            }
        }

        //so that item in listview gets highlighted
        public int ChatId
        {
            get => this.SelectedChat.Id;
            set
            {
                this.SelectedChat = ViewData.FirstOrDefault(f => f.Id == value);
            }
        }

        public event ChatSelectedEventHandler ChatSelected;

        public override async void ReloadData()
        {
            this.preLoad();
            var newChats = await apiClient.GetChatsAsync(false);
            this.postLoad(newChats);
        }

        internal void ReloadData2()
        {
            this.preLoad();
            var newChats = apiClient.GetChats(false);
            this.postLoad2(newChats);
        }

        private void postLoad2(List<Chat> newChats)
        {
            if (newChats != null)
            {
                List<Chat> toAdd = new List<Chat>();
                List<Chat> toRemove = new List<Chat>();

                foreach (var newItem in newChats)
                    if (!this.ViewData.Any(a => a.CompareTo(newItem) == 0))
                        toAdd.Add(newItem);

                foreach (var oldItem in ViewData)
                    if (!newChats.Any(a => a.CompareTo(oldItem) == 0))
                        toRemove.Add(oldItem);


                
                    foreach (var item in toRemove)
                        ViewData.Remove(item);
                    foreach (var item in toAdd)
                        ViewData.Add(item);

                    DataStatus = DataLoadingStatus.Loaded;
                    this.refreshTimer.Start();
            }
            else
            {
                DataStatus = DataLoadingStatus.Loaded;
                this.refreshTimer.Start();
            }
        }
    }
}
