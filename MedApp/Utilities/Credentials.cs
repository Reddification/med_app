﻿namespace MedApp.ApiClient
{
    public class Credentials
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
