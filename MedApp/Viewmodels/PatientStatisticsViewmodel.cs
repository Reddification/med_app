﻿using LiveCharts;
using LiveCharts.Wpf;
using MedApp.ApiClient;
using MedApp.DomainObjects;
using MedApp.DomainObjects.ViewObjects;
using MedApp.View.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace MedApp.View.Viewmodels
{
    //TODO: extract an abstract class for both patients and chats viewmodels
    /// <summary>
    /// For personal statistics I guess
    /// </summary>
    class PatientStatisticsViewmodel : DataLoadingViewmodel<MedProbe>, IViewChanger
    {
        private PatientListViewmodel patientListViewmodel;

        public DataTable StatisticData = new DataTable();

        public DataTable ChartDataSource = new DataTable();

        private string tbcount;

        public string TbCount
        {
            get => tbcount;
            set
            {
                this.tbcount = value;
                updateProperty(nameof(tbcount));
            }
        }

        private int oldPatID;

        public int OldPatID
        {
            get => oldPatID;
            set
            {
                this.oldPatID = value;
                updateProperty(nameof(oldPatID));
            }
        }

        private DateTime fiterdatestart;
        public DateTime FiterDateStart
        {
            get => fiterdatestart;
            set
            {
                this.fiterdatestart = value;
                updateProperty(nameof(fiterdatestart));
            }
        }

        private DateTime fiterdateend;
        public DateTime FiterDateEnd
        {
            get => fiterdateend;
            set
            {
                this.fiterdateend = value;
                updateProperty(nameof(fiterdateend));
            }
        }

        private SeriesCollection seriescollection;
        public SeriesCollection SeriesCollection
        {
            get => seriescollection;
            set
            {
                this.seriescollection = value;
                updateProperty(nameof(seriescollection));
            }
        }

        private SeriesCollection seriescollection2;
        public SeriesCollection SeriesCollection2
        {
            get => seriescollection2;
            set
            {
                this.seriescollection2 = value;
                updateProperty(nameof(seriescollection2));
            }
        }

        private SeriesCollection seriescollection3;
        public SeriesCollection SeriesCollection3
        {
            get => seriescollection3;
            set
            {
                this.seriescollection3 = value;
                updateProperty(nameof(seriescollection3));
            }
        }

        private SeriesCollection seriescollection4;
        public SeriesCollection SeriesCollection4
        {
            get => seriescollection4;
            set
            {
                this.seriescollection4 = value;
                updateProperty(nameof(seriescollection4));
            }
        }

        private SeriesCollection seriescollection5;
        public SeriesCollection SeriesCollection5
        {
            get => seriescollection5;
            set
            {
                this.seriescollection5 = value;
                updateProperty(nameof(seriescollection5));
            }
        }

        private SeriesCollection seriescollection6;
        public SeriesCollection SeriesCollection6
        {
            get => seriescollection6;
            set
            {
                this.seriescollection6 = value;
                updateProperty(nameof(seriescollection6));
            }
        }

        internal void OpenStatisticsById(int userId)
        {
            var user = this.PatientListViewmodel.ViewData.FirstOrDefault(f => f.UserId == userId);
            if (user != null)
                this.PatientListViewmodel.SelectedPatient = user;
        }

        private SeriesCollection seriescollection7;
        public SeriesCollection SeriesCollection7
        {
            get => seriescollection7;
            set
            {
                this.seriescollection7 = value;
                updateProperty(nameof(seriescollection7));
            }
        }

        private string[] labels;
        public string[] Labels
        {
            get => labels;
            set
            {
                this.labels = value;
                updateProperty(nameof(labels));
            }
        }
        private Func<string, string> yformatter;
        public Func<string, string> YFormatter
        {
            get => yformatter;
            set
            {
                this.yformatter = value;
                updateProperty(nameof(yformatter));
            }
        }

        private List<string> probelist = new List<string>() {"Weight (kg)",
                                                        "Height (cm)",
                                                        "Calories eaten",
                                                        "Calories burn",
                                                        "Waist (cm)",
                                                        "Walking time (min)",
                                                        "Exercies time (min)"};

        public PatientStatisticsViewmodel(IApiClient ac, IEnumerable<Profile> patients) : base(ac, 2000)
        {
            PatientListViewmodel = new PatientListViewmodel(ac, patients);
            PatientListViewmodel.PatientSelected += PatientListViewmodel_PatientSelected;
            PatientListViewmodel.PatientId = patients.FirstOrDefault().Id;
            this.OpenChatCommand = new LambdaCommand<object>(OpenChat, (o) => true);
        }

        public ICommand OpenChatCommand { get; set; }
        public PatientListViewmodel PatientListViewmodel
        {
            get => patientListViewmodel;
            private set
            {
                this.patientListViewmodel = value;
                //updateProperty(nameof(PatientListViewmodel));
            }
        }

        public string PersonName => this.PatientListViewmodel.SelectedPatient?.Name;

        private void OpenChat(object p)
        {
            RequestWindowChange?.Invoke(this, new RequestWindowChangeEventArgs(WindowType.Chats, PatientListViewmodel.SelectedPatient?.UserId));
        }

        public event RequestWindowChangeEventHandler RequestWindowChange;

        private void PatientListViewmodel_PatientSelected(Profile selectedPatient)
        {
            updateProperty(nameof(PersonName));
        }

        public override void Open()
        {
            base.Open();
            this.PatientListViewmodel.Open();
        }

        public async override void ReloadData()
        {
            preLoad();
            StatisticData.Clear();
            if (StatisticData.Columns.Count == 0)
            {
                FiterDateEnd = DateTime.Now;
                FiterDateStart = DateTime.Now.AddDays(-7);

                StatisticData.Columns.Add("Probe");
                StatisticData.Columns.Add("Value");
                StatisticData.Columns.Add("Date");

                ChartDataSource.Columns.Add("Value");
                ChartDataSource.Columns.Add("Date");
            }

            var newProbes = await this.apiClient.GetMedProbesAsync(PatientListViewmodel.SelectedPatient?.UserId ?? 0, false);
            //var newChats = this.apiClient.GetChatMessages(activePersonId);
            postLoad(newProbes);
            // MessageBox.Show(newProbes.Count() + "****");
            foreach (var row in newProbes)
            {
                StatisticData.Rows.Add(row.ProbeType, row.Value, row.Captured);
            }
            // MessageBox.Show(StatisticData.Rows.Count + "---");
            // if (StatisticData.Rows.Count > 0)
            CreateChart();
        }

        public void CreateChart()
        {
            App.Current.Dispatcher.BeginInvoke((Action)delegate
            {
                int k = 0;
                foreach (string Probe in probelist)
                {
                   
                    SeriesCollection StatChart = new SeriesCollection();

                    ChartDataSource.Clear();

                    for (int i = 0; i < StatisticData.Rows.Count; i++)
                    {
                        if (Probe.Trim().Contains(StatisticData.Rows[i]["Probe"].ToString().Trim()) && DateTime.Parse(StatisticData.Rows[i]["Date"].ToString()) >= FiterDateStart && DateTime.Parse(StatisticData.Rows[i]["Date"].ToString()) <= FiterDateEnd)
                        {
                            ChartDataSource.Rows.Add(StatisticData.Rows[i]["Value"].ToString(), StatisticData.Rows[i]["Date"].ToString());
                        }
                    }
                    k++;
                   // if (ChartDataSource.Rows.Count.ToString() != TbCount)
                    {
                        
                     if(k == 6) TbCount = ChartDataSource.Rows.Count.ToString();

                        var newSort = ChartDataSource.AsEnumerable()
                                .Select(g => new
                                {
                                    Date = g.Field<string>("Date"),
                                    Value = g.Field<string>("Value")
                                }).OrderBy(x => x.Date);


                        SolidColorBrush brush = new SolidColorBrush(Colors.Green);
                        SolidColorBrush fillbrush = new SolidColorBrush(Colors.LightGreen);
                        if (Probe == "Weight (kg)")
                        {
                            brush = new SolidColorBrush(Colors.Blue);
                            fillbrush = new SolidColorBrush(Colors.LightBlue);
                        }
                        if (Probe == "Height (cm)")
                        {
                            brush = new SolidColorBrush(Colors.Green);
                            fillbrush = new SolidColorBrush(Colors.LightGreen);
                        }
                        if (Probe == "Calories eaten")
                        {
                            brush = new SolidColorBrush(Colors.Orange);
                            fillbrush = new SolidColorBrush(Colors.Yellow);
                        }
                        if (Probe == "Calories burn")
                        {
                            brush = new SolidColorBrush(Colors.Red);
                            fillbrush = new SolidColorBrush(Colors.LightCoral);
                        }
                        if (Probe == "Walking time (min)")
                        {
                            brush = new SolidColorBrush(Colors.Purple);
                            fillbrush = new SolidColorBrush(Colors.LightPink);
                        }
                        if (Probe == "Exercies time (min)")
                        {
                            brush = new SolidColorBrush(Colors.Blue);
                            fillbrush = new SolidColorBrush(Colors.LightBlue);
                        }


                        StatChart = new SeriesCollection
                                    {
                                     new LineSeries
                                     {
                                        Title = Probe,
                                        Values = new ChartValues<int>(newSort.Select(c=>int.Parse(c.Value))),
                                        Fill = fillbrush,
                                        Stroke = brush,
                                     }
                                    };


                        Labels = newSort.Select(c => DateTime.Parse(c.Date).ToString("dd.MM.yyyy")).ToArray();

                        YFormatter = value => value.ToString();

                        if (Probe == "Weight (kg)") SeriesCollection = StatChart;
                        if (Probe == "Height (cm)") SeriesCollection2 = StatChart;
                        if (Probe == "Calories eaten") SeriesCollection3 = StatChart;
                        if (Probe == "Calories burn") SeriesCollection4 = StatChart;
                        if (Probe == "Waist (cm)") SeriesCollection5 = StatChart;
                        if (Probe == "Walking time (min)") SeriesCollection6 = StatChart;
                        if (Probe == "Exercies time (min)") SeriesCollection7 = StatChart;

                    }

                    //SeriesCollection = new SeriesCollection
                    //    {
                    //    new LineSeries
                    //    {
                    //        Title = "Series 1",
                    //        Values = new ChartValues<double> { 4, 6, 5, 2 ,4 }
                    //    }
                    //    };

                    //Labels = new[] { "Jan", "Feb", "Mar", "Apr", "May" };
                    //YFormatter = value => value.ToString();
                }
            });
        }

        public override void Close()
        {
            base.Close();
            this.PatientListViewmodel.Close();
        }
    }
}
