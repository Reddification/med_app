﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ApiObjects
{
    public class Chat
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("members")]
        //THESE ARE USER IDS NOT PROFILE IDS
        public int[] Members { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
