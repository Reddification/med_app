﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.DomainObjects.ApiObjects
{
    public class Profile
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("user")]
        public int User { get; set; } //id 
        [JsonProperty("role")]
        public int Role { get; set; }//id
        //URL
        [JsonProperty("img")]
        public string Img { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("date_of_birth")]
        public DateTime DateOfBirth { get; set; }
    }
}
