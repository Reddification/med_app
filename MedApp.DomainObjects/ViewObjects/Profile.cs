﻿using System;

namespace MedApp.DomainObjects.ViewObjects
{
    public class Profile:IComparable
    {
        public int Id { get; set; }
        public int UserId { get; set; }//profiles and users are separated WTF
        public string Name { get; set; }
        public byte[] Photo { get; set; }
        public Gender Gender { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string RoleTitle { get; set; }
        public int RoleId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age => (int)Math.Floor((DateTime.Now - DateOfBirth).TotalDays / 365);
        public int CompareTo(object obj)
        {
            if (!(obj is Profile))
                return -1;

            if ((obj as Profile).Id == this.Id)
                return 0;
            else return (obj as Profile).Id < this.Id ? 1 : -1;
        }
    }
}
