﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Wpf;
namespace MedApp.View
{
    public partial class PatientStatistics : UserControl
    {
        //public static DataTable StatisticData = new DataTable();
        //public static DataTable ChartDataSource = new DataTable();
        //List<string> ProbeList = new List<string>() {"Weight (kg)",
        //                                                "Height (cm)",
        //                                                "Calories eaten",
        //                                                "Calories burn",
        //                                                "Waist (cm)",
        //                                                "Walking time (min)",
        //                                                "Exercise (min)"};
        //DateTime StartDate;
        //DateTime EndDate;

        public PatientStatistics()
        {
            InitializeComponent();
            //DatePickerStart.SelectedDate = DateTime.Today.AddDays(-7);
            //DatePickerEnd.SelectedDate = DateTime.Today;
            //try
            //{
            //    StatisticData.Columns.Add("Probe");
            //    StatisticData.Columns.Add("Value");
            //    StatisticData.Columns.Add("Date");
            //    StatisticData.Columns.Add("User");

            //    ChartDataSource.Columns.Add("Value");
            //    ChartDataSource.Columns.Add("Date");
            //}
            //catch { }

            //StartDate = DateTime.Today.AddDays(-7);
            //EndDate = DateTime.Today;
         

            //if (StatisticData.Rows.Count == 0)
            //{
            //    Random rand = new Random();

            //    for (int i = 0; i < 60; i++)
            //    {
            //        DateTime now = DateTime.Now.AddDays(i * (-1));

            //        StatisticData.Rows.Add(new object[] { ProbeList[0], rand.Next(13, 120).ToString(), now, "user1" });
            //        StatisticData.Rows.Add(new object[] { ProbeList[1], rand.Next(143, 212).ToString(), now, "user1" });
            //        StatisticData.Rows.Add(new object[] { ProbeList[2], rand.Next(300, 4500).ToString(), now, "user1" });
            //        StatisticData.Rows.Add(new object[] { ProbeList[3], rand.Next(300, 4500).ToString(), now, "user1" });
            //        StatisticData.Rows.Add(new object[] { ProbeList[4], rand.Next(40, 300).ToString(), now, "user1" });
            //        StatisticData.Rows.Add(new object[] { ProbeList[5], rand.Next(40, 300).ToString(), now, "user1" });
            //        StatisticData.Rows.Add(new object[] { ProbeList[6], rand.Next(40, 300).ToString(), now, "user1" });
            //    }
            //    CreateChart();
            //}
        }

        //public void CreateChart()
        //{

        //    foreach (string Probe in ProbeList)
        //    {

        //        CartesianChart StatChart = new CartesianChart();
        //        if (Probe == "Weight (kg)") StatChart = ChartWeight;
        //        if (Probe == "Height (cm)") StatChart = ChartHeight;
        //        if (Probe == "Calories eaten") StatChart = ChartCaloriesEaten;
        //        if (Probe == "Calories burn") StatChart = ChartCaloriesBurn;
        //        if (Probe == "Waist (cm)") StatChart = ChartWaist;
        //        if (Probe == "Walking time (min)") StatChart = ChartWalking;
        //        if (Probe == "Exercise (min)") StatChart = ChartExercise;
        //        ChartDataSource.Clear();

        //        for (int i = 0; i < StatisticData.Rows.Count; i++)
        //        {
        //            if (StatisticData.Rows[i]["Probe"].ToString() == Probe && DateTime.Parse(StatisticData.Rows[i]["Date"].ToString()) >= StartDate && DateTime.Parse(StatisticData.Rows[i]["Date"].ToString()) <= EndDate)
        //            {
        //                ChartDataSource.Rows.Add(StatisticData.Rows[i]["Value"].ToString(), StatisticData.Rows[i]["Date"].ToString());
        //            }
        //        }

        //        var newSort = ChartDataSource.AsEnumerable()
        //                .Select(g => new
        //                {
        //                    Date = g.Field<string>("Date"),
        //                    Value = g.Field<string>("Value")
        //                }).OrderBy(x => x.Date);

        //        try
        //        {
        //            StatChart.AxisX.Clear();
        //            StatChart.AxisY.Clear();
        //            StatChart.Series.Clear();
        //        }
        //        catch { };

        //        SolidColorBrush brush = new SolidColorBrush(Colors.Green);
        //        SolidColorBrush fillbrush = new SolidColorBrush(Colors.LightGreen);
        //        if (Probe == "Weight (kg)")
        //        {
        //            brush = new SolidColorBrush(Colors.Blue);
        //            fillbrush = new SolidColorBrush(Colors.LightBlue);
        //        }
        //        if (Probe == "Height (cm)")
        //        {
        //            brush = new SolidColorBrush(Colors.Green);
        //            fillbrush = new SolidColorBrush(Colors.LightGreen);
        //        }
        //        if (Probe == "Calories eaten")
        //        {
        //            brush = new SolidColorBrush(Colors.Orange);
        //            fillbrush = new SolidColorBrush(Colors.Yellow);
        //        }
        //        if (Probe == "Calories burn")
        //        {
        //            brush = new SolidColorBrush(Colors.Red);
        //            fillbrush = new SolidColorBrush(Colors.LightCoral);
        //        }
        //        if (Probe == "Walking time (min)")
        //        {
        //            brush = new SolidColorBrush(Colors.Purple);
        //            fillbrush = new SolidColorBrush(Colors.LightPink);
        //        }
        //        if (Probe == "Exercise (min)")
        //        {
        //            brush = new SolidColorBrush(Colors.Blue);
        //            fillbrush = new SolidColorBrush(Colors.LightBlue);
        //        }

         
        //        StatChart.Series = new SeriesCollection
        //        {
        //        new LineSeries
        //        {
        //            Title = Probe,
        //            Values = new ChartValues<int>(newSort.Select(c=>int.Parse(c.Value))),
        //            Stroke=brush,
        //            Fill = fillbrush
        //        }
        //        };

        //        StatChart.AxisX.Add(new Axis
        //        {
        //            Title = "Date",
        //            Labels = newSort.Select(c => DateTime.Parse(c.Date).ToString("dd.MM.yyyy")).ToArray()
        //        });

        //        StatChart.AxisY.Add(new Axis
        //        {
        //            Title = "Probe value",
        //            LabelFormatter = value => value.ToString()
        //        });


        //        StatChart.LegendLocation = LegendLocation.Top;


        //    }
        //}

        private void DatePickerStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{

            //    StartDate = DateTime.Parse(DatePickerStart.SelectedDate.ToString());
            //    CreateChart();
            //}
            //catch { }
        }

        private void DatePickerEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    EndDate = DateTime.Parse(DatePickerEnd.SelectedDate.ToString());
            //    CreateChart();
            //}
            //catch { }
        }
    }
}
