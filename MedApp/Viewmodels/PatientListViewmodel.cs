﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;
using MedApp.ApiClient;
using MedApp.DomainObjects;
using MedApp.DomainObjects.ViewObjects;

namespace MedApp.View.Viewmodels
{
    public delegate void PatientSelectedEventHandler(Profile selectedPatient);

    public class PatientListViewmodel : DataLoadingViewmodel<Profile>
    {
        private Profile selectedPatient;
        private int patientId;
        private string searchstring;
        //public string SearchString
        //{
        //    get => searchstring;
        //    set
        //    {
        //        this.searchstring = value;
        //        updateProperty(nameof(searchstring));
        //    }
        //}


        public PatientListViewmodel(IApiClient ac, IEnumerable<Profile> users) : base(ac, 2000)
        {
            this.ViewData = new ObservableCollection<Profile>(users);
        }

        

        public override void ReloadData()
        {
            this.preLoad();
            var newData = apiClient.GetContactPersonList(true);
            this.postLoad(newData);
        }

        //public ObservableCollection<Profile> Patients { get; set; }

        public Profile SelectedPatient 
        {
            get => this.selectedPatient;
            set
            {
                this.selectedPatient = value;
                updateProperty(nameof(SelectedPatient));
                PatientSelected?.Invoke(value);
            }
        }

        //so that item in listview gets highlighted
        public int PatientId 
        {
            get => this.patientId;
            set
            {
                this.patientId = value;
                this.SelectedPatient = ViewData.FirstOrDefault(f => f.UserId == value);
            }
        }

        public event PatientSelectedEventHandler PatientSelected;

        ~PatientListViewmodel()
        {
            this.refreshTimer.Dispose();
        }
    }
}
