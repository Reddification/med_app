﻿using MedApp.DomainObjects.ViewObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.Cache
{
    public class CachedStatistics : CachedData<Statistic>
    {
        public CachedStatistics(TimeSpan lifetime) : base(lifetime)
        {
        }
    }
}
