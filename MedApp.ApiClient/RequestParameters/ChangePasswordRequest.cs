﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedApp.ApiClient.RequestParameters
{
    public class ChangePasswordRequest
    {
        [JsonProperty("old_password")]
        public string OldPassword { get; set; }

        [JsonProperty("password")]
        public string NewPassword { get; set; }

        [JsonProperty("password_confirm")]
        public string NewPasswordRepeat { get; set; }
    }
}
