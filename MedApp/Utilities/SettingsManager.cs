﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedApp.View.Utilities
{
    public class SettingsManager
    {
        public bool SavePassword(string newPassword)
        {
            bool needSaving = Properties.Settings.Default.RememberedPassword != newPassword;
            if (needSaving)
                Properties.Settings.Default.RememberedPassword = newPassword;

            return needSaving;
        }

        public string RemindPassword() => Properties.Settings.Default.RememberedPassword;

        public bool SaveUsername(string newUsername)
        {
            bool needSaving = Properties.Settings.Default.RememberedLogin != newUsername;
            if (needSaving)
                Properties.Settings.Default.RememberedLogin = newUsername;

            return needSaving;
        }

        public string RemindUsername() => Properties.Settings.Default.RememberedLogin;

        public bool SaveReminders(bool save)
        {
            bool needSaving = Properties.Settings.Default.RememberMe != save;
            if (needSaving)
            {
                Properties.Settings.Default.RememberMe = save;
                if (!save)
                {
                    Properties.Settings.Default.RememberedLogin = "";
                    Properties.Settings.Default.RememberedPassword = "";
                    Properties.Settings.Default.ApiURL = "";
                }
            }

            return needSaving;
        }

        public bool NeedReminders() => Properties.Settings.Default.RememberMe;

        public bool SaveServerAddress(string newAddress)
        {
            bool needSaving = Properties.Settings.Default.ApiURL != newAddress;
            if (needSaving)
                Properties.Settings.Default.ApiURL = newAddress;
            return needSaving;
        }

        public void CommitSettings()
        {
            Properties.Settings.Default.Save();
        }

        public string RemindServerAddress() => Properties.Settings.Default.ApiURL;
    }
}
