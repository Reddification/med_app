﻿using System;
using System.Windows.Input;

namespace MedApp.View.Utilities
{
    public class LambdaCommand<T> : ICommand
    {
        private Action<T> action;
        private Func<T, bool> canExecute;
        public event EventHandler CanExecuteChanged;

        public LambdaCommand(Action<T> a, Func<T, bool> ce)
        {
            if (a == null)
                throw new ArgumentNullException("a");
            this.action = a;
            this.canExecute = ce;
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true :canExecute((T)parameter);
        }

        public void Execute(object parameter)
        {
            action((T)parameter);
        }
    }
}
