﻿using MedApp.ApiClient;
using MedApp.View;
using MedApp.View.Properties;
using MedApp.View.Viewmodels;
using System.Windows;
using Settings = MedApp.View.Properties.Settings;

namespace MedApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LoginViewmodel lvm;
        private MainViewmodel mcvm;
        //private RegistrationViewmodel rgvm;
        private IApiClient apiClient;
        public MainWindow()
        {
            InitializeComponent();
            //#if DEBUG
            //            apiClient = new TestApiClient();
            //#else
            //            apiClient = new MedApiClient(Settings.Default.ApiURL);
            //#endif
            apiClient = new MedApiClient(Settings.Default.ApiURL);
            lvm = new LoginViewmodel(apiClient);
            lvm.RequestWindowChange += Lvm_RequestWindowChange;
            this.AppContainer.DataContext = lvm;
        }

        private void Mcvm_RequestWindowChange(object sender, View.Utilities.RequestWindowChangeEventArgs e)
        {
            switch (e.TargetWindow)
            {
                case View.Utilities.WindowType.Login:
                case View.Utilities.WindowType.Logout:
                    this.AppContainer.Content = lvm;
                    break;
                default:
                    break;
            }
        }

        //TODO: perhaps come up with some navigation manager because right now this logic is done in 2 different entities
        private void Lvm_RequestWindowChange(object sender, View.Utilities.RequestWindowChangeEventArgs e)
        {
            switch (e.TargetWindow)
            {
                case View.Utilities.WindowType.Login:
                    this.AppContainer.Content = lvm;
                    break;
                case View.Utilities.WindowType.Main:
                    mcvm = new MainViewmodel(apiClient);
                    this.mcvm.RequestWindowChange += Mcvm_RequestWindowChange;
                    this.AppContainer.Content = mcvm;
                    break;
                case View.Utilities.WindowType.Register:
                    //this.AppContainer.Content = rgvm;
                    break;
                default:
                    break;
            }
        }
    }
}
