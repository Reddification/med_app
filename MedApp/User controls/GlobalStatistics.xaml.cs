﻿using System.Windows.Controls;
using System.Collections;
using LiveCharts;
using LiveCharts.Wpf;
using System.Data;
using System.Collections.Generic;
using System;
using System.Linq;
using System.IO;

namespace MedApp.View
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class GlobalStatistics : UserControl
    {
        //public static DataTable StatisticData = new DataTable();
        //public static DataTable ChartDataSource = new DataTable();

        //public static SeriesCollection SeriesCollection { get; set; }
        //public static int[] Labels { get; set; }
        //public static Func<int, int> YFormatter { get; set; }


        //public static string Probe;
        //public static int MinAge;
        //public static int MaxAge;
        //public static string Gender;

        //List<string> ProbeList = new List<string>() {"Weight (kg)",
        //                                                "Height (cm)",
        //                                                "Calories eaten",
        //                                                "Calories burn",
        //                                                "Waist (cm)",
        //                                                "Walking time (min)",
        //                                                "Exercise (min)"};

        //List<string> GenderList = new List<string>() { "All", "Male", "Female" };

        public GlobalStatistics()
        {
            InitializeComponent();


            //try
            //{
            //    StatisticData.Columns.Add("Probe");
            //    StatisticData.Columns.Add("Value");
            //    StatisticData.Columns.Add("Age");
            //    StatisticData.Columns.Add("Gender");

            //    ChartDataSource.Columns.Add("Value");
            //    ChartDataSource.Columns.Add("Age");
            //}
            //catch { }

            //for (int i = 13; i < 86; i++)
            //{
            //    FilterMinAge.Items.Add(i);
            //    FilterMaxAge.Items.Add(i);
            //}


            //FilterMinAge.SelectedIndex = 0;
            //FilterMaxAge.SelectedIndex = FilterMaxAge.Items.Count - 1;

            //FilterParameter.ItemsSource = ProbeList;
            //FilterParameter.SelectedIndex = 0;
            //Probe = FilterParameter.SelectedItem.ToString();

            //FilterGender.ItemsSource = GenderList;
            //FilterGender.SelectedIndex = 0;
            //Gender = FilterGender.SelectedItem.ToString();

            //if (StatisticData.Rows.Count == 0)
            //{
            //    Random rand = new Random();
            //    for (int i = 0; i < 1000; i++)
            //    {
            //        StatisticData.Rows.Add(new object[] { ProbeList[i % ProbeList.Count], rand.Next(40, 300).ToString(), rand.Next(13, 85).ToString(), GenderList[i % GenderList.Count] });
            //        if (StatisticData.Rows[StatisticData.Rows.Count - 1]["Probe"].ToString() == "Weight (kg)")
            //        {
            //            StatisticData.Rows[StatisticData.Rows.Count - 1]["Value"] = rand.Next(13, 120);
            //        }
            //        if (StatisticData.Rows[StatisticData.Rows.Count - 1]["Probe"].ToString() == "Height (cm)")
            //        {
            //            StatisticData.Rows[StatisticData.Rows.Count - 1]["Value"] = rand.Next(143, 212);
            //        }
            //        if (StatisticData.Rows[StatisticData.Rows.Count - 1]["Probe"].ToString().Contains("Calories"))
            //        {
            //            StatisticData.Rows[StatisticData.Rows.Count - 1]["Value"] = rand.Next(300, 4500);
            //        }
            //    }
            //    CreateChart();
            //}
        }



        //private void FilterMinAge_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    MinAge = int.Parse(FilterMinAge.SelectedItem.ToString());
        //    CreateChart();
        //}

        //private void FilterMaxAge_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    MaxAge = int.Parse(FilterMaxAge.SelectedItem.ToString());
        //    CreateChart();
        //}

        //private void FilterGender_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Gender = FilterGender.SelectedItem.ToString();
        //    CreateChart();
        //}

        //private void FilterParameter_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        //{
        //    Probe = FilterParameter.SelectedItem.ToString();
        //    CreateChart();
        //}
    }
}
